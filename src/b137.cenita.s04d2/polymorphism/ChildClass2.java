package b137.cenita.s04d2.polymorphism;

public class ChildClass2 extends DynamicPolymorphism {

    public void message() {
        System.out.println("I am the child class 2!");
    }
}
