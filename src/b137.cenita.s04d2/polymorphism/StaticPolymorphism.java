package b137.cenita.s04d2.polymorphism;

public class StaticPolymorphism {

    public int add(int a, int b) {
        return a + b;
    }

    // Overload the add method by requiring another parameters
    public int add(int a, int b, int c) {
        return a + b + c;
    }

    // Overload the add method by changing the data type of the parameters
    public double add(double a, double b) {
        return a + b;
    }
}
