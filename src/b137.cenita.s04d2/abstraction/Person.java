package b137.cenita.s04d2.abstraction;

public class Person implements Actions, SpecialSkills {

    public Person() {}

    // Methods from Actions interface

    public void sleep() {
        Actions.super.sleep();
    }

    public void run() {
        Actions.super.run();
    }

    // Methods from SpecialSkills interface

    public void computerProgram() {
        SpecialSkills.super.computerProgram();
    }

    public void driveACar() {
        SpecialSkills.super.driveACar();
    }
}
