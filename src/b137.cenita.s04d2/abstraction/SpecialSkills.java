package b137.cenita.s04d2.abstraction;

public interface SpecialSkills {

    default void computerProgram() {
        // Implementation
        System.out.println("I can program in Java!");
    };

    default void driveACar() {
        // Implementation
        System.out.println("I can drive a car!");
    };
}
