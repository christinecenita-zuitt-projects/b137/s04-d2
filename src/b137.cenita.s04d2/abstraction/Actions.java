package b137.cenita.s04d2.abstraction;

public interface Actions {

    default void sleep() {
        // Implementation
        System.out.println("Zzz...");
    };

    default void run() {
        // Implementation
        System.out.println("Running...");
    };
}
