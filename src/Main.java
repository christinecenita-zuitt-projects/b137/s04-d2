package b137.cenita.s04d2;

import b137.cenita.s04d2.abstraction.AnotherPerson;
import b137.cenita.s04d2.abstraction.Person;
import b137.cenita.s04d2.inheritance.Animal;
import b137.cenita.s04d2.inheritance.Dog;
import b137.cenita.s04d2.polymorphism.ChildClass1;
import b137.cenita.s04d2.polymorphism.ChildClass2;
import b137.cenita.s04d2.polymorphism.DynamicPolymorphism;
import b137.cenita.s04d2.polymorphism.StaticPolymorphism;

public class Main {

    public static void main(String[] args) {
        System.out.println("Inheritance, Abstraction, and Polymorphism\n");

        Animal firstAnimal = new Animal("Peechy", "white");

        firstAnimal.showDetails();

        Dog firstDog = new Dog("Poochy", "brown", "Aspin");

        firstDog.bark();

        System.out.println(firstDog.getBreed());
        System.out.println(firstDog.getName());

        firstDog.showDetails();

        System.out.println(); // Abstraction

        Person firstPerson = new Person();

        firstPerson.sleep();
        firstPerson.run();
        firstPerson.computerProgram();
        firstPerson.driveACar();

        System.out.println();

        AnotherPerson secondPerson = new AnotherPerson();

        secondPerson.sleep();
        secondPerson.run();
        secondPerson.computerProgram();
        secondPerson.driveACar();

        System.out.println();

        StaticPolymorphism poly = new StaticPolymorphism();

        System.out.println(poly.add(1, 2));
        System.out.println(poly.add(1, 2, 3));
        System.out.println(poly.add(1.5, 2.4));

        System.out.println(); // Polymorphism

        DynamicPolymorphism poly2;

        poly2 = new DynamicPolymorphism();
        poly2.message();

        poly2 = new ChildClass1();
        poly2.message();

        poly2 = new ChildClass2();
        poly2.message();
    }
}
